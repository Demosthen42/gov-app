# GOV-app

GUI for GOV-express

### Installing

To init project

```bash
cd gov-app

npm install -g ionic

npm install
```

### Run

To run project

```bash
ionic serve
```

import { Component } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { GovBaseService } from '../services/gov-base.service';
import { ZakList, Zak, Zak_text } from '../models/zak-list';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage {
  constructor (
    public api:GovBaseService,
    public loadingController: LoadingController,
    public navCtrl: NavController
    ) {}
  data: ZakList[];
  data1: ZakList[];
  zak: Zak;
  zakText: Zak_text;

  ionViewDidEnter() {
    this.getZakList();
  }

  async getZakList() {
    const loading = await this.loadingController.create({
      message: 'Загрузка списка законов'
    });
    await loading.present();
    this.api.getZakList()
      .subscribe(res => {
        this.data1 = res;
        this.data = this.data1;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  onSearchChange(event){
    var val = event.target.value;
    this.data = this.data1;
    if (val && val.trim() != '') {
      this.data = this.data.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  onClick(nd:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd
      }
    };
    this.navCtrl.navigateForward(['zakinfo'], navigationExtras);
  }

}

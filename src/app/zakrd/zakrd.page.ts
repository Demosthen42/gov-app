import { Component } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { GovBaseService } from '../services/gov-base.service';
import { Zak_text } from '../models/zak-list';
import { ActivatedRoute, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-zakrd',
  templateUrl: './zakrd.page.html',
  styleUrls: ['./zakrd.page.scss'],
})
export class ZakrdPage {

  zakText:Zak_text;
  nd:string;
  rd:string;
  show:boolean = false;
  text:string;


  constructor(
    public api:GovBaseService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params =>{
      this.nd = params.nd;
      this.rd = params.rd
   });
  }

  ionViewDidEnter() {
    this.getZak_text(this.nd, this.rd);
    
  }


  cleanView() {
    //this.text[0] = this.zakText.rd_list[0].text;
    
  }
  async getZak_text(nd:string, rd:string) {
    const loading = await this.loadingController.create({
      message: 'Загрузка текста редакции'
    });
    await loading.present();
    this.api.getZakText(nd, rd)
      .subscribe(res => {
        this.zakText = res;   
        this.text=this.zakText.rd_list[0].text;
        this.cleanView();
    this.show = true;     
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }  

  onBack(nd:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd
      }
    };
    this.navCtrl.navigateBack(['zakinfo'], navigationExtras);
  }
}

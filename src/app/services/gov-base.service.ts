import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ZakList, Zak, Zak_text, NewZak, ZakAdd, diff, ZakInfo } from '../models/zak-list';

@Injectable({
  providedIn: 'root'
})


export class GovBaseService {
  
  url = 'http://185.178.44.189:5000/api/v1';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  
  getZakList():Observable<ZakList[]>
  {
    return this.http.get<ZakList[]>(this.url+'/listzak').pipe();
  }

  getZak(nd:string):Observable<Zak>
  {
    return this.http.get<Zak>(this.url+'/listzak/'+nd).pipe();
  }

  getZakText(nd:string, rd: string):Observable<Zak_text>
  {
    return this.http.get<Zak_text>(this.url+'/listzak/'+nd+'/'+rd).pipe();
  }

  getDiff(nd:string, f: string, s:string):Observable<diff>
  {
    return this.http.get<diff>(this.url+'/diff?nd='+nd+'&f='+f+'&s='+s).pipe();
  }

  postFindZak(st:string):Observable<NewZak[]>
  {
    let myHeaders = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<NewZak[]>(this.url+'/find', {'st':st}, myHeaders).pipe();
  }

  postNewZak(nd:number):Observable<ZakAdd>
  {
    let myHeaders = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<ZakAdd>(this.url+'/add_zak', {'nd':nd}, myHeaders).pipe();
  }

  getZakInfo(nd:string):Observable<ZakInfo>
  {
    return this.http.get<ZakInfo>(this.url+'/info?nd='+nd).pipe();
  }

  getZakPreview(nd:string):Observable<string>
  {
    return this.http.get<string>(this.url+'/preview/'+nd).pipe();
  }

}

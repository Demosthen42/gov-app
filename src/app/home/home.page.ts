import { Component } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { GovBaseService } from '../services/gov-base.service';
import { NewZak } from '../models/zak-list';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  st:string;
  data: NewZak[];
  ans: String;
  show:boolean = false;
  constructor (
    public alertController: AlertController,
    public api:GovBaseService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute
    ) {}

  async getZak(st:string) {
    const loading = await this.loadingController.create({
      message: 'Поиск закона'
    });
    await loading.present();
    this.api.postFindZak(st)
      .subscribe(res => {
        this.data = res;
        this.show = true;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  async presentAlert(ans: String) {
    const alert = await this.alertController.create({
      header: 'Закон добавлен!',
      message: ans.toString(),
      buttons: ['OK']
    });

    await alert.present();
  }

  async postNewZak(nd:number) {
    const loading = await this.loadingController.create({
      message: 'Добавление закона'
    });
    await loading.present();
    this.api.postNewZak(nd)
      .subscribe(res => {
        console.log(res);
        this.ans = res.ans;
        this.presentAlert(this.ans);
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }
  
  onClick(){
    this.getZak(this.st);
  }

  addNew(nd:number){
    this.postNewZak(nd);
  }

  onPreview(nd:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd,
      }
    };
    this.navCtrl.navigateForward(['preview'], navigationExtras);
  }

}

import { Component } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { GovBaseService } from '../services/gov-base.service';
import { diff } from '../models/zak-list';
import { ActivatedRoute, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-diff',
  templateUrl: './diff.page.html',
  styleUrls: ['./diff.page.scss'],
})
export class DiffPage {

  data: diff;
  nd:string;
  f:string;
  s:string;
  show:boolean = false;
  constructor(
    public api:GovBaseService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params =>{
      this.nd = params.nd;
      this.f = params.f;
      this.s = params.s;
   });
   }
  ionViewDidEnter() {
    this.getDiff();
  }
  async getDiff() {
    const loading = await this.loadingController.create({
      message: 'Загрузка списка изменений'
    });
    await loading.present();
    this.api.getDiff(this.nd, this.f, this.s)
    //this.api.getDiff('102045166', '0', '1')
      .subscribe(res => {
        this.data = res;
        this.show = true;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }
  onBack(nd:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd
      }
    };
    this.navCtrl.navigateBack(['zakinfo'], navigationExtras);
  }
}

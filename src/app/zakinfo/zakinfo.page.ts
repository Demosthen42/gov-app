import { Component } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { GovBaseService } from '../services/gov-base.service';
import { Zak } from '../models/zak-list';
import { ActivatedRoute, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-zakinfo',
  templateUrl: './zakinfo.page.html',
  styleUrls: ['./zakinfo.page.scss'],
})
export class ZakinfoPage {

  constructor(
    public api:GovBaseService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    if (this.show == false){
      this.route.queryParams.subscribe(params =>{
        this.nd = params.nd;
      })
    }
    
   }
  zak: Zak;
  nd:string;
  show:boolean = false;
  f:number;
  s:number;

  ionViewDidEnter() {
    if (this.show == false){
      this.getZak(this.nd);
    }
  }
  //Загрузка и информации о законе
  async getZak(nd:string) {
    const loading = await this.loadingController.create({
      message: 'Загрузка информации о законе'
    });
    await loading.present();
    this.api.getZak(nd)
      .subscribe(res => {
        this.zak = res;
        this.show = true;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }
  //Переход к просмотру редакции
  onClick(nd:number, rd:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd,
          rd: rd
      }
    };
    this.navCtrl.navigateForward(['zakrd'], navigationExtras);
  }
  //Переход к diff
  onDiff(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: this.nd,
          f: this.f,
          s: this.s
      }
    };
    this.navCtrl.navigateForward(['diff'], navigationExtras);
  }
  //Переход к просмотру связей
  onConnections(nd:string){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd,
      }
    };
    this.navCtrl.navigateForward(['zakconnections'], navigationExtras);
  }
}
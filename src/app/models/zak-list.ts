export class ZakList {
    name: string;
    nd: number;
    rds: number;
}

export class Zak {
    name: string;
    nd: number;
    rds: number;
    rd_list: {
        rd: number;
        date: string
    }
}

export class Zak_text {
    _id: string;
    rd_list: {
        _id: string;
        rd: number;
        date: string;
        text: string;
    }
}

export class NewZak {
    zak: string;
    name: string;
    nd: number;
}

export class ZakAdd {
    ans: string;
}

export class diff {
    diff_mas:{
        stat: string,
        diff :{
            r:number;
            text:string;
        }
    };
    new_stat: {
        stat:string;
        text:string;
    }
}

export class ZakInfo {
    ref:{
        dop:string;
        name:string;
        zak:string;
        nd:number;
    };
    rvref:{
        dop:string;
        name:string;
        zak:string;
        nd:number;
    };
}
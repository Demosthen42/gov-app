import { Component } from '@angular/core';

import { LoadingController, NavController } from '@ionic/angular';
import { GovBaseService } from '../services/gov-base.service';
import { ActivatedRoute, NavigationExtras } from "@angular/router";
@Component({
  selector: 'app-preview',
  templateUrl: './preview.page.html',
  styleUrls: ['./preview.page.scss'],
})
export class PreviewPage {

  constructor(
    public api:GovBaseService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params =>{
      this.nd = params.nd;
    })
   }
   nd:string;
   text: string;
   show: boolean = false;

   ionViewDidEnter() {
    this.getZakPreview(this.nd);
   }

   async getZakPreview(nd:string) {
    const loading = await this.loadingController.create({
      message: 'Поиск закона'
    });
    await loading.present();
    this.api.getZakPreview(nd)
      .subscribe(res => {
        this.text = res;
        this.show = true;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

   onBack(){
    this.navCtrl.pop();
  }
}

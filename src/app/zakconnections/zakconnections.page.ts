import { Component } from '@angular/core';

import { LoadingController, NavController } from '@ionic/angular';
import { GovBaseService } from '../services/gov-base.service';
import { ZakInfo } from '../models/zak-list';
import { ActivatedRoute, NavigationExtras } from "@angular/router";
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-zakconnections',
  templateUrl: './zakconnections.page.html',
  styleUrls: ['./zakconnections.page.scss'],
})
export class ZakconnectionsPage {

  constructor(
    public api:GovBaseService,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public alertController: AlertController,
    public route: ActivatedRoute
  ) {
    if (this.show == false){
      this.route.queryParams.subscribe(params =>{
        this.nd = params.nd;
      })
    }
    
  }

  nd:string = '';
  info: ZakInfo;
  show: boolean = false;
  ans: String;

  ionViewDidEnter() {
    if (this.show == false){
      this.getZakInfo(this.nd);
    }
    
  }

  async getZakInfo(nd:string) {
    const loading = await this.loadingController.create({
      message: 'Загрузка информации о ссылках на закон'
    });
    await loading.present();
    this.api.getZakInfo(nd)
      .subscribe(res => {
        this.info = res;
        this.show = true;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  onBack(nd:string){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd
      }
    };
    this.navCtrl.pop();
  }

  async postNewZak(nd:number) {
    const loading = await this.loadingController.create({
      message: 'Добавление закона'
    });
    await loading.present();
    this.api.postNewZak(nd)
      .subscribe(res => {
        console.log(res);
        this.ans = res.ans;
        this.presentAlert(this.ans);
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  async presentAlert(ans: String) {
    const alert = await this.alertController.create({
      header: 'Закон добавлен!',
      message: ans.toString(),
      buttons: ['OK']
    });

    await alert.present();
  }

  onPreview(nd:number){
    let navigationExtras: NavigationExtras = {
      queryParams: {
          nd: nd,
      }
    };
    this.navCtrl.navigateForward(['preview'], navigationExtras);
  }

}
